# Ansible / GitLab CI Example

### Overview

The purpose of this GitLab project is to document using GitLab CI tools with Ansible.

### Getting Started

- As an example, the play will look up a pokemon on pokeapi.co using its REST API
